# collectd-huawei-wwan

This package collects signal statistics with collectd. It uses the modem tty
defined in uci via: `uci get network.wwan.tty`.

## Usage
1. Install `collectd-mod-exec`, `useradd` and `sudo`
2. Create a new user for statistics collection: `useradd collectd-mod-exe`
3. Configure sudo rights using `visudo`. Add the line: 
   `collectd-mod-exec ALL=(root) NOPASSWD:SETENV: /usr/share/collectd-huawei-wwan/collect`
4. In the web interface, go to Statistics -> Setup -> General plugins -> Exec
   and add a command. Script is `/usr/share/collectd-huawei-wwan/collect`, user
   and group are `collectd-mod-exec`
5. Visualise the data. This can be done by installing the `exec.lua` script in
    `/usr/lib/lua/luci/statistics/rrdtool/definitions/exec.lua`
