module("luci.statistics.rrdtool.definitions.exec", package.seeall)

function rrdargs(graph, plugin, plugin_instance)
    -- For $HOSTNAME/exec-foo-bar/temperature_baz-quux.rrd, plugin will be
    -- "exec" and plugin_instance will be "foo-bar".  I guess graph will be
    -- "baz-quux"?  We may also be ignoring a fourth argument, dtype.
    if "huawei-wwan" == plugin_instance then
        local signal = {
            title = "%H: WWAN signal",
            vlabel = "%",
            data = {
                types = { "percent" },
                options = {
                    percent = {
                        title  = "Signal",
                    }
                }
            }
        }
	local rssi = {                                                                                                                
	    per_instance=true,
            title = "%H: WWAN RSSI",                                                                                                  
            vlabel = "dBm",                                                                                                               
		
            data = {                                                                                                                    
                types = { "signal_power" },                                                                                                  
                options = {                                                                                                             
                    signal_power__rssi = {                                                                                                         
                        title  = "RSSI",                                                                                              
                    }                                                                                                                   
                }                                                                                                                       
            }                                                                                                                           
        }  

	return { signal, rssi }
    end
end
